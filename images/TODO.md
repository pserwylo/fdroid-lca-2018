Required screenshots

F-Droid tutorial:

 * Image of user wanting to get apps on their phone from the internet

 * Search for apps
 * Notification of updates
 * Browse categories
 * Add repository
 * Localization

Security model:

 * Signed metadata
   + Image of offline signing
   + Show comprimise of web server to show that HTTPs is not enough
 * Repo fingerprints
   + Trust on first use
   + Fingerprint embedded in QR code
 * Android security


